# LIG Backend Exam
## Installation Steps
**Requirements**

 - [Python](https://www.python.org/) version 3.8 or latest
 - [Virtualenv](https://virtualenv.pypa.io/en/latest/)
 - [PostgreSQL](https://www.postgresql.org/) version 12

**Clone the repo**
```
git clone https://gitlab.com/jorje.b/backend-exam.git
cd backend-exam
```
**Create a new virtual environment**
```
virtualenv -p `which python3` venv
```
**Activate the virtual environment and then install the dependencies**
```
. venv/bin/activate
pip install -r requirements.txt
```
**Create the database**
```
sudo -u postgres psql -c "CREATE ROLE <username> LOGIN PASSWORD '<password>' SUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;"
sudo -u postgres createdb --echo --owner=<username> <database name>
```
**Create the .env file**
```
cp .env.example .env
```
**Configure PostgreSQL connection details and secret key in .env**
```
SECRET_KEY=

POSTGRES_DB=
POSTGRES_USER=
POSTGRES_PASSWORD=
POSTGRES_HOST=localhost
POSTGRES_PORT=5432
```
**Run database migrations**
```
cd api
python manage.py migrate
```
**Create a superuser**
```
python manage.py createsuperuser --email <email> --username <username>
```
## Running the application
```
python manage.py runserver
```
Before you can test the REST API, you will have to create first an OAuth application here [http://localhost:8000/o/applications/](http://localhost:8000/o/applications/).

I included a POSTMAN collection that you can use to test the REST API.

`LIG_Backend_Exam.postman_collection.json`

To generate an access token on POSTMAN:

 1. Edit the **Articles API** folder.
 2. Then go to the **Authorization** tab.
 3. Select **OAuth 2.0** as the authorization type, then click on the **Get New Access Token** button.
 4. Set the following values for each field:
	 - Access Token URL: **http://localhost:8000/o/token/**
	 - Grant Type: **Password Credential**
	 - Scopes: **articles**
	 - Client Authentication: **Send as Basic Auth header**
	 - Username, Password, Client ID, Client Secret
 5. Then click on the **Request Token** button.
 6. Click on the **Use Token** button to set the token as global.
 7. To use the token, make sure you have set the Authorization type of each request to **Inherit auth from parent**.

 You can now test the REST API.

**Running the unit tests**
```
python manage.py test
```

