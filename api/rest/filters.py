from dry_rest_permissions.generics import DRYPermissionFiltersBase


class ArticleFilterBackend(DRYPermissionFiltersBase):
    def filter_list_queryset(self, request, queryset, view):
        """
        Limit articles to those only created by the author.
        """
        return queryset.filter(author=request.user)
