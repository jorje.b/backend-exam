from django.contrib.auth.hashers import make_password
from rest.models import User, Article
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        validated_data['password'] = make_password(
            validated_data['password']
        )
        return super(UserSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        validated_data['password'] = make_password(
            validated_data['password']
        )
        return super(UserSerializer, self).update(instance, validated_data)

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'password',
        ]
        extra_kwargs = {
            'password': {'write_only': True}
        }


class ArticleSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        validated_data['author'] = self.context['request'].user
        return super(ArticleSerializer, self).create(validated_data)

    class Meta:
        model = Article
        fields = [
            'id',
            'author',
            'title',
            'slug',
            'content',
        ]
        read_only_fields = ('author',)
