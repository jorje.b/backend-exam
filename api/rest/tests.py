from django.test import TestCase
from django.urls import reverse
from oauth2_provider.models import Application
from rest.models import User, Article
from rest_framework import status
from rest_framework.test import (
    APITestCase,
    APIClient,
)

import json


class ArticleTests(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.content_type = 'application/json'
        user_data = {
            'username': 'gvanrossum',
            'email': 'python-creator@localhost',
            'password': 'ultraelectromagnetictop',
            'first_name': 'Guido',
            'last_name': 'Van Rossum'
        }
        response = self.client.post(
            reverse('user-list'),
            data=user_data
        )
        self.assertEqual(response.status_code, 201)
        self.user = User.objects.get(email=user_data['email'])
        self.application = Application(
            name='Test Application',
            user_id=self.user.id,
            client_type=Application.CLIENT_PUBLIC,
            authorization_grant_type=Application.GRANT_PASSWORD,
        )
        self.application.save()
        token_request_data = {
            'grant_type': 'password',
            'username': user_data['username'],
            'password': user_data['password'],
            'client_id': self.application.client_id
        }
        response = self.client.post(
            reverse('oauth2_provider:token'),
            data=token_request_data, secure=True)
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content.decode('utf-8'))
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' +
                                content['access_token'])

    def tearDown(self):
        self.application.delete()

    def test_create_article(self):
        data = {
            'title': 'Test Article',
            'slug': 'test-article',
            'content': 'Test article content'
        }
        response = self.client.post(
            reverse('article-list'),
            data,
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Article.objects.count(), 1)
        article = Article.objects.get()
        self.assertEqual(article.title, data['title'])
        self.assertEqual(article.slug, data['slug'])
        self.assertEqual(article.content, data['content'])

    def test_update_article(self):
        data = {
            'title': 'Test Article',
            'slug': 'test-article',
            'content': 'Test article content'
        }
        self.client.post(
            reverse('article-list'),
            data,
            format='json'
        )
        article = Article.objects.get()
        updated_data = {
            'title': 'Test Article 2',
            'slug': 'test-article-2',
            'content': 'Test article 2 content'
        }
        response = self.client.put(
            reverse('article-detail', kwargs={'pk': article.id}),
            updated_data,
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Article.objects.count(), 1)
        article = Article.objects.get()
        self.assertEqual(article.title, updated_data['title'])
        self.assertEqual(article.slug, updated_data['slug'])
        self.assertEqual(article.content, updated_data['content'])

    def test_delete_article(self):
        data = {
            'title': 'Test Article',
            'slug': 'test-article',
            'content': 'Test article content'
        }
        self.client.post(
            reverse('article-list'),
            data,
            format='json'
        )
        article = Article.objects.get()
        response = self.client.delete(
            reverse('article-detail', kwargs={'pk': article.id})
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Article.objects.count(), 0)
        self.assertEqual(response.content.decode('utf-8'), '')
