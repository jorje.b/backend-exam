from dry_rest_permissions.generics import DRYPermissions
from oauth2_provider.contrib.rest_framework import (
    IsAuthenticatedOrTokenHasScope
)
from rest.filters import ArticleFilterBackend
from rest.models import User, Article
from rest.serializers import UserSerializer, ArticleSerializer
from rest_framework import (
    mixins,
    viewsets,
)


class UserViewSet(mixins.CreateModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.DestroyModelMixin,
                  viewsets.GenericViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [
        DRYPermissions,
    ]


class ArticleViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows articles to be viewed or edited.
    """
    queryset = Article.objects.all().order_by('-created_at')
    serializer_class = ArticleSerializer
    filter_backends = (ArticleFilterBackend,)
    required_scopes = ['articles']
    permission_classes = [
        IsAuthenticatedOrTokenHasScope,
        DRYPermissions,
    ]
